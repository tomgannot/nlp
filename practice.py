import spacy
import csv
import pandas as pd
from pathlib import Path
from spacy.kb import KnowledgeBase

def load_entities():
    entities_loc = "/home/tom/nlp/nbastats.csv"  # distributed alongside this notebook

    names = dict()
    position = dict()
    with entities_loc.open("r", encoding="utf8") as csvfile:
        csvreader = csv.reader(csvfile, delimiter=",")
        for row in csvreader:
            qid = row[0]
            name = row[1]
            desc = row[2]
            names[qid] = name
            position[qid] = desc
    return names, position

if __name__ == "__main__":
    #text = "Google and Apple are gonna release new phones next year"
    print("Enter CSV or Excel path")
    file = input()
    
    nlp = spacy.load('en_core_web_trf')
    #doc = nlp(text)
    name_dict, desc_dict = load_entities()

    kb = KnowledgeBase(vocab=nlp.vocab, entity_vector_length=300)

    for qid, desc in desc_dict.items():
     desc_doc = nlp(desc)
     desc_enc = desc_doc.vector
     kb.add_entity(entity=qid, entity_vector=desc_enc, freq=342)   # 342 is an arbitrary value here

    for qid, name in name_dict.items():
        kb.add_alias(alias = name, entities = [qid], probabilities = [1])